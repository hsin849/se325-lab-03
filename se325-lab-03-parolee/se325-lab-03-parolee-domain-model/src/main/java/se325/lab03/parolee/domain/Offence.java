package se325.lab03.parolee.domain;

public enum Offence {
    THEFT, POSSESION_OF_OFFENSIVE_WEAPON, MURDER, RAPE, TAX_EVASION;
}
